import './styles.css';

export const ProfilePhoto = ({ photo }) => (
    <div className='profilePhotoContainer'>
        <img src={photo} />
    </div>
)