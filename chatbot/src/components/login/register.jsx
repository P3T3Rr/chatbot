import { faTriangleExclamation, faUpload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CR from '../../assets/contries/cr.png';
import { getUser } from '../../services/get';
import { Spinner } from '../spinner/spinner';
import { NewUser } from '../../services/set';
import logo from '../../assets/ico.png';
import { useState } from 'react';
import './styles.css';

export const Register = ({ setRegister, setAlertLogin }) => {

    const [alertMessage, setAlertMessage] = useState("");
    const [alert, setAlert] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [imageURL, setImageURL] = useState(logo);
    const [image, setImage] = useState();

    const register = () => {
        const number = "+506 " + document.getElementById("registerPhone").value;
        const name = document.getElementById("registerName").value;
        const mail = document.getElementById("registerMail").value;
        const password = document.getElementById("registerPassword").value;
        if (number === "+506 " || name === "" || mail === "" || password === "") {
            setAlertMessage("Campos Vacíos");
            setAlert(true);
        }
        else {
            setSpinner(true);
            let data = getUser(number);
            data.then(function (user) {
                if (user === undefined) {
                    NewUser(name, number, mail, password, image, setSpinner);
                    setAlertLogin(false);
                    setRegister(false);
                }
                else {
                    setAlertMessage("Ya está registrado");
                    setAlert(true);
                    setSpinner(false);
                }
            })
        }
    }

    const addImage = (e) => {
        const file = e.currentTarget.files[0];
        let url = URL.createObjectURL(file);
        setImageURL(url)
        setImage(file)
    }

    return (
        <div className='loginPage'>
            <div className='loginContainer' id='registerContainer'>
                <h2>Registrar Cuenta</h2>
                <div>
                    <div className='profilePhotoContainerRegister'>
                        <img src={imageURL} />
                    </div>
                    <div className='upload_image_register'>
                        <label className='upload_button' title='Cargar Imagen'>
                            <FontAwesomeIcon icon={faUpload} id="uploadIcon"/>
                            <input hidden type="file" accept='image/*' onChange={addImage}></input>
                        </label>
                    </div>
                </div>

                <form action="">
                    <input type="text" id='registerName' placeholder='Nombre' />
                    <div className='numberContainerLogin'>
                        <div id="CR"><img src={CR} /></div>
                        <p>+506</p>
                        <input type="number" id='registerPhone' placeholder='Número de teléfono' />
                    </div>
                    <input type="text" id='registerMail' placeholder='Correo electrónico' />
                    <input type="password" id='registerPassword' placeholder='Contraseña' />
                </form>

                {spinner ?
                    <div id='spinnerLoggin'><Spinner /></div>
                    : null
                }
                {alert ?
                    <div><p className='alertMessage' id='logginAlertMessage'><FontAwesomeIcon id='iconAlert' icon={faTriangleExclamation} />{alertMessage}</p></div>
                    :
                    <div><p className='alertMessage' id='logginAlertMessage'>{alertMessage}</p></div>
                }

                <div className='loginButtons'>
                    <button onClick={() => setRegister(false)} className='registerButton' title='Iniciar Sesión'>Cancelar</button>
                    <button onClick={register} className='loginButton' title='Registrarse'>Crear Cuenta</button>
                </div>
            </div>
        </div>
    )
}
