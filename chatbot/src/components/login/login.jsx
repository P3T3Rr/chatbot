import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useNavigate } from 'react-router-dom';
import CR from '../../assets/contries/cr.png';
import { Spinner } from '../spinner/spinner';
import { getUser } from '../../services/get';
import logo from '../../assets/ico.png';
import { Register } from './register';
import { useState } from 'react';
import './styles.css';

export const Login = ({ setLogged }) => {

    const navigate = useNavigate();
    const [alertMessage, setAlertMessage] = useState("");
    const [alert, setAlert] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [register, setRegister] = useState(false);

    const save = () => {
        const number = "+506 " + document.getElementById("loginPhone").value;
        const password = document.getElementById("loginPassword").value;
        if (number === "+506 " || password === "") {
            setAlertMessage("Campos Vacíos");
            setAlert(true);
        }
        else {
            setSpinner(true);
            let data = getUser(number);
            data.then(function (user) {
                if (user != undefined) {
                    if (password == user.password) {
                        navigate("/");
                        localStorage.setItem("user", JSON.stringify(number));
                        setLogged(true);
                    }
                    else {
                        setAlertMessage("Contraseña incorrecta");
                        setAlert(true);
                    }
                }
                else {
                    setAlertMessage("No está registrado");
                    setAlert(true);
                }
                setSpinner(false);
            })
        }
    }

    return (
        <>
            {!register ?
                <div className='loginPage'>
                    <div className='loginContainer'>
                        <img src={logo} />
                        <h2>Chat Bot</h2>

                        <form action="">
                            <div className='numberContainerLogin'>
                                <div id="CR"><img src={CR} /></div>
                                <p>+506</p>
                                <input type="number" id='loginPhone' placeholder='Número de teléfono' />
                            </div>
                            <input type="password" id='loginPassword' placeholder='Contraseña' />
                        </form>

                        {spinner ?
                            <div id='spinnerLoggin'><Spinner /></div>
                            : null
                        }
                        {alert ?
                            <div><p className='alertMessage' id='logginAlertMessage'><FontAwesomeIcon id='iconAlert' icon={faTriangleExclamation} />{alertMessage}</p></div>
                            :
                            <div><p className='alertMessage' id='logginAlertMessage'>{alertMessage}</p></div>
                        }

                        <div className='loginButtons'>
                            <button onClick={()=>setRegister(true)} className='registerButton' title='Registrarse'>Registrarse</button>
                            <button onClick={save} className='loginButton' title='Iniciar Sesión'>Iniciar Sesión</button>
                        </div>
                    </div>
                </div>
                :
                <Register setRegister={setRegister} setAlertLogin={setAlert} />
            }
        </>
    )
}
