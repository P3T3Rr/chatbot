import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

export const Modal = ({ close, DeleteMessage }) => {
    return (
        <div className='modal' id='modalDelete'>
            <div className="modal__container" id='modalDeleteContainer'>
                <button onClick={close} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <h2>Confirmación</h2>
                <div className='messageConfirmer_deleteMessage'>
                    ¿Eliminar mensaje completamente?
                </div>
                <div className='modalDeleteButtons'>
                    <button onClick={close} className='cancel' title='Cancelar'>Cancelar</button>
                    <button onClick={DeleteMessage} className='save' title='Eliminar Mensaje'>Eliminar</button>
                </div>
            </div>
        </div>
    )
}
