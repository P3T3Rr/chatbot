import { collection, onSnapshot, query, where, doc, updateDoc } from 'firebase/firestore';
import { createMessageAdapter } from '../../../adapters/message-adapter';
import { createChatsAdapter } from '../../../adapters/chat-adapter';
import { createUserAdapter } from '../../../adapters/user-adapter';
import { deleteMessage } from '../../../services/delete';
import { InputMessage } from '../input-message/input';
import { db } from '../../../services/firebaseConfig';
import { useEffect, useState, useRef } from 'react';
import { MessageMenu } from './messages-menus/menu';
import { Spinner } from '../../spinner/spinner';
import { useParams } from 'react-router-dom';
import { HeaderChat } from '../header-chat';
import { Modal } from './modal';
import './stylesAudio.css';
import './styles.css'

export const Chat = () => {
    const init = () => {
        const UserLogged = localStorage.getItem("user");
        return UserLogged ? JSON.parse(UserLogged) : [];
    }

    const UserLogged = init();
    const idChat = useParams().id;
    const urlphone = useParams().phone;
    const scrollRef = useRef();
    const [spinner, setSpinner] = useState(true);
    const [user, setUser] = useState();
    const [chat, setChat] = useState();
    const [messages, setMessages] = useState();
    const [messagesFilter, setMessagesFilter] = useState();
    const [blocked, setBlocked] = useState(false);
    const [userLogged, setUserLogged] = useState();
    const [confirmDelete, setConfirmDelete] = useState(false);
    const [confirmEdit, setConfirmEdit] = useState(false);
    const [message, setMessage] = useState();
    const [floatingMenu, setFloatingMenu] = useState(false);

    useEffect(() => {
        functionGetUserLoggued();
        functionGetUser();
        functionGetChat();
        functionGetMessages();
        scrollRef.current?.scrollIntoView();
    }, [idChat])

    useEffect(() => {
        scrollRef.current?.scrollIntoView();
    }, [messages, messagesFilter])

    const close = () => {
        setConfirmDelete(false);
    }

    const messageEdit = (m) => {
        setMessage(m);
        document.getElementById('input').textContent = m.message;
        setConfirmEdit(true);
    }

    const DeleteMessage = () => {
        deleteMessage(message.id);
        setMessage(null);
        setConfirmDelete(false);
    }

    const goToChats = () => {
        window.location.hash = "no-back-button";
        window.location.hash = "Again-No-back-button"
        window.onhashchange = function () {
            window.location.hash = "no-back-button";
        }
    }

    const functionGetUser = () => {
        const snap = query(collection(db, "users"), where("phone", "==", urlphone));
        onSnapshot(snap, (querySnapshot) => {
            console.log("-------------------------\nLlamo a un user del chat: ");
            const users = [];
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    users.push(createUserAdapter(doc.data(), doc.id));
                    setUser(users[0]);
                    setBlocked(false);
                    if (users[0].blockeds !== []) {
                        users[0].blockeds.map(phone => {
                            if (phone == UserLogged) {
                                setBlocked(true);
                            }
                        })
                    }
                }
                else {
                    setUser(null);
                }
                console.log(users[0]);
            })
        });
    }

    const functionGetMessages = () => {
        const q = query(collection(db, "messages"), where("chat", "==", idChat));
        onSnapshot(q, (querySnapshot) => {
            console.log("id del chat ..." + idChat + "...");
            console.log("-------------------------\nLlamo a los mensajes: ");
            const arrayMessages = [];
            querySnapshot.forEach((doc) => {
                arrayMessages.push(createMessageAdapter(doc.data(), doc.id));
            });
            if (arrayMessages.length !== 0) {
                arrayMessages.sort(((a, b) => a.dateTimestamp - b.dateTimestamp))
                setMessages(arrayMessages);
                setMessagesFilter(arrayMessages);
                setSpinner(false);
                scrollRef.current?.scrollIntoView();
            }
            else {
                setMessages(null);
                setMessagesFilter(null);
                setSpinner(false)
            }
            console.log(arrayMessages);
        });
    }

    const functionGetChat = () => {
        const q = query(collection(db, "chats"), where('__name__', '==', idChat));
        onSnapshot(q, (querySnapshot) => {
            const newChat = [];
            querySnapshot.forEach((doc) => {
                newChat.push(createChatsAdapter(doc.id, doc.data()));
            });
            console.log("-------------------------\nEste es el chat");
            console.log(newChat[0]);
            if (newChat.length !== 0) {
                setChat(newChat[0]);
                readMessages(newChat[0]);
            }
            else {
                setChat(null);
            }
        });
    }

    const functionGetUserLoggued = () => {
        const snap = query(collection(db, "users"), where("phone", "==", UserLogged));
        onSnapshot(snap, (querySnapshot) => {
            console.log("-------------------------\nLlamo al user logueado en el chat: ");
            const users = [];
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    users.push(createUserAdapter(doc.data(), doc.id));
                    setUserLogged(users[0]);
                }
                else {
                    setUserLogged(null);
                }
                console.log(users[0]);
            })
        })
    }

    const readMessages = async (chat) => {
        const newList = []
        if (chat.news.length > 0) {
            Object.keys(chat.news).forEach(i => {
                if (chat.news[i].user === UserLogged) {
                    newList.push(chat.news[i])
                }
            })
            console.log(idChat);
            await updateDoc(doc(db, "chats", idChat), { news: newList });
        }
    }

    return (
        <div className='column2' onLoad={goToChats}>
            {confirmDelete ?
                <Modal close={close} DeleteMessage={DeleteMessage} />
                :
                null
            }

            {user != null ?
                <HeaderChat chat={chat} user={user} blocked={blocked} setMessagesFilter={setMessagesFilter} messages={messages} userLogged={userLogged} floatingMenu={floatingMenu} setFloatingMenu={setFloatingMenu} />
                :
                <div className='headerChat' />
            }

            <div className={`chatContainer ${spinner ? "s" : ""}`} onClick={() => setFloatingMenu(false)}>

                {spinner ?
                    <div id='spinner'>
                        <Spinner />
                    </div>
                    :
                    messagesFilter !== null ?
                        <div>
                            {messagesFilter.map((message, index) =>
                                <div key={index} ref={scrollRef}>
                                    {message.user === UserLogged ?
                                        <div className={"messageContainerUser1"} key={index}>
                                            <div className='messageOption'>
                                                <MessageMenu message={message} setConfirmDelete={setConfirmDelete} setMessage={setMessage} messageEdit={messageEdit} />
                                            </div>
                                            {message.type == "image" ?
                                                <p id='imageP'>
                                                    <a href={message.url} target="_blank">
                                                        <img src={message.url} className="imageMessage" />
                                                        <span id='hourImage'>{message.time}</span>
                                                    </a>

                                                </p>
                                                :
                                                message.type == "video" ?
                                                    <p id='imageP'>
                                                        <video src={message.url} controls className="imageMessage" ></video>
                                                        <span id='hourImage'>{message.time}</span>
                                                    </p>
                                                    :
                                                    message.type == "audio" ?
                                                        <div className='audioContainerUser1'>
                                                            <div className='containerAudio'>
                                                                <audio id='audioControler1' src={message.url} className="audioMessage" preload="true" controls></audio>
                                                            </div>
                                                            <span id='hourAudio'>{message.time}</span>
                                                        </div>
                                                        :
                                                        <pre>
                                                            {message.message}
                                                            <span id='hour'>{message.time}</span>
                                                        </pre>
                                            }
                                            <span className={"cornerMessageUser1"} />
                                        </div>
                                        :
                                        <div className={"messageContainerUser2"} key={index}>
                                            <span className={"cornerMessageUser2"} />
                                            {message.type == "image" ?
                                                <p id='imageP'>
                                                    <a href={message.url} target="_blank">
                                                        <img src={message.url} className="imageMessage" />
                                                        <span id='hourImage'>{message.time}</span>
                                                    </a>

                                                </p>
                                                :
                                                message.type == "video" ?
                                                    <p id='imageP'>
                                                        <video src={message.url} controls className="imageMessage" ></video>
                                                        <span id='hourImage'>{message.time}</span>
                                                    </p>
                                                    :
                                                    message.type == "audio" ?
                                                        <div className='audioContainerUser2'>
                                                            <div className='containerAudio'>
                                                                <audio id='audioControler2' src={message.url} className="audioMessage" preload="true" controls></audio>
                                                            </div>
                                                            <span id='hourAudio'>{message.time}</span>
                                                        </div>
                                                        :
                                                        <pre>
                                                            {message.message}
                                                            <span id='hour'>{message.time}</span>
                                                        </pre>
                                            }
                                        </div>
                                    }
                                </div>
                            )}
                        </div>
                        :
                        <div className='noMessages'><h1>No hay mensajes</h1></div>
                }
            </div>

            {!blocked ?
                <InputMessage chat={chat} idChat={idChat} userLogged={UserLogged} user={userLogged} confirmEdit={confirmEdit} setConfirmEdit={setConfirmEdit} message={message} setMessage={setMessage} setFloatingMenu={setFloatingMenu} />
                :
                <div className='inputBlocked'>¡Este usuario te ha bloqueado!</div>
            }
        </div>
    )
}