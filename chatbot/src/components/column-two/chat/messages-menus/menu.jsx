import { faTrash, faPen } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './styles.css';

export const MessageMenu = ({ message, setConfirmDelete, setMessage, messageEdit }) => {

    const Delete = () => {
        setMessage(message);
        setConfirmDelete(true);
    }

    const Update = () => {
        messageEdit(message);
    }

    return (
        <>
            <div onClick={Delete} className='deleteIconMessage' title='Eliminar Mensaje'>
                <FontAwesomeIcon icon={faTrash} />
            </div>
            {message.type == "text" ?
                <div onClick={Update} className='editIconMessage' title='Editar Mensaje'>
                    <FontAwesomeIcon icon={faPen} />
                </div>
                :
                null
            }
        </>
    )
}