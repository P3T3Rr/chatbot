import { getDate } from '../../../../validations/getDate';
import { useEffect, useState } from 'react';
import './styles.css';

export const TemporaryMessageMenu = ({ setDateTimeDelete, setAutoDelete, AutoDelete, dateTimeDelete, autoDelete }) => {

    const [dateMin, setDateMin] = useState("");

    useEffect(() => {

        const date = getDate();
        setDateMin(date)

        if (dateTimeDelete != '') {
            const dateTime = dateTimeDelete.split("~");
            document.getElementById("switch-label").checked = true;
            document.getElementById("date").value = dateTime[0];
            document.getElementById("time").value = dateTime[1];
        }
        else
            document.getElementById("switch-label").checked = false;
    }, [])


    const selectDateTime = () => {
        const Date = document.getElementById("date").value
        const Time = document.getElementById("time").value

        if (Date !== '' && Time !== '') {
            if (document.getElementById("switch-label").checked != true) {
                setAutoDelete(!autoDelete);
                setDateTimeDelete('')
                document.getElementById("switch-label").checked = false;
                AutoDelete()
            }
            else {
                setAutoDelete(!autoDelete);
                const d = Date + "~" + Time
                setDateTimeDelete(d)
                document.getElementById("switch-label").checked = true;
                AutoDelete()
            }
        }
        else {
            setAutoDelete(true);
            setDateTimeDelete('')
            document.getElementById("switch-label").checked = false;
        }
    }

    const updateDateTime = () => {
        const Date = document.getElementById("date").value
        const Time = document.getElementById("time").value
        if (Date !== '' && Time !== '' && document.getElementById("switch-label").checked == true) {
            const d = Date + "~" + Time
            setDateTimeDelete(d)
        }
        else {
            setAutoDelete(true);
            setDateTimeDelete('')
            document.getElementById("switch-label").checked = false;
        }
    }

    return (
        <div className='floatingMenu' id='autoeliminationMenu'>
            <div className='containerDate'>
                <p>Autoeliminación:</p>
            </div>
            <div className='containerDate'>
                <input min={dateMin} onChange={updateDateTime} title='Seleccionar día' id='date' type="date" className='date' />
            </div>
            <div className='containerDate'>
                <input onChange={updateDateTime} title='Seleccionar hora' id='time' type="time" className='date' />
            </div>
            <div className="twoColunms">
                <div className="switch-button">
                    <input onChange={selectDateTime} type="checkbox" name="switch-button" id="switch-label" className="switch-button__checkbox" />
                    <label htmlFor="switch-label" className="switch-button__label"></label>
                </div>
                <p>Mensaje temporal</p>
            </div>
            <br />
        </div>
    )
}