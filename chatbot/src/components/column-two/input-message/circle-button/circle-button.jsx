import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Send } from '../../../../validations/inputMessage';
import { useParams } from 'react-router-dom';
import './styles.css';

export const CircleButton = ({ chat, icon, setEmpty, user, autoDelete, setDateTimeDelete, setAutoDelete }) => {
    const init = () => {
        const UserLogged = localStorage.getItem("user");
        return UserLogged ? JSON.parse(UserLogged) : [];
    }

    const UserLogged = init();
    const idChat = useParams().id;

    const OnClick = () => {
        Send(idChat, chat, UserLogged, user, setEmpty, autoDelete);
        setDateTimeDelete('');
        setAutoDelete(false);
    }

    return (
        <div className='circleButton' onClick={OnClick}>
            <FontAwesomeIcon id='icon' icon={icon} />
        </div>
    )
}