import { faImage, faVideo, faMusic } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { Spinner } from '../../../spinner/spinner';
import { setFile } from '../../../../services/set';
import { useState } from 'react';
import './styles.css';
import { updateStadistics } from '../../../../services/update';
import { getStadistics } from '../../../../services/get';

export const FloatingMenu = ({ chat, idChat, user, userLogged, closeFloatingMenu }) => {

    const [confirmation, setConfirmation] = useState(false);
    const [files, setFiles] = useState([]);
    const [spinner, setSpinner] = useState(false);
    const [fileType, setFileType] = useState();

    const confirmationFunc = (e) => {
        setSpinner(true);
        if (files[0].file.type === "video/mp4") {
            let stadistics = getStadistics(userLogged);
            stadistics.then(function (result) {
                result[0] = result[0] + 1
                result[2] = result[2] + 1
                setFile(files, chat, idChat, userLogged, "video", closeFloatingMenu, setSpinner, setConfirmation);
                updateStadistics(user.id, result)
            })
        }
        else if (files[0].file.type === "audio/mpeg") {
            let stadistics = getStadistics(userLogged);
            stadistics.then(function (result) {
                result[0] = result[0] + 1
                result[3] = result[3] + 1
                setFile(files, chat, idChat, userLogged, "audio", closeFloatingMenu, setSpinner, setConfirmation)
                updateStadistics(user.id, result)
            })
        }
        else {
            let stadistics = getStadistics(userLogged);
            stadistics.then(function (result) {
                result[0] = result[0] + 1
                result[1] = result[1] + 1
                setFile(files, chat, idChat, userLogged, "image", closeFloatingMenu, setSpinner, setConfirmation)
                updateStadistics(user.id, result)
            })
        }
        setFiles([])
    }

    const close = () => {
        setFiles([])
        setConfirmation(false);
        closeFloatingMenu(false);
    }

    const addImage = (e) => {
        setConfirmation(true);

        let indexImg;
        if (files.length > 0)
            indexImg = files[files.length - 1].index + 1;
        else
            indexImg = 0;

        let newImgsToState = readmultifiles(e, indexImg);

        console.log(newImgsToState[0].file.type);

        if (newImgsToState[0].file.type === "video/mp4") {
            setFiles(newImgsToState);
            setFileType("video");
        }
        else if (newImgsToState[0].file.type === "audio/mpeg") {
            setFiles(newImgsToState);
            setFileType("audio");
        }
        else {
            if (fileType !== "video" && fileType !== "audio") {
                let newImage = [...newImgsToState, ...files];
                setFiles(newImage);
                setFileType("image")
            }
            else {
                setFiles(newImgsToState);
                setFileType("image");
            }
        }

    }

    const readmultifiles = (e, indexInicial) => {
        const files = e.currentTarget.files;
        const arrayImages = [];
        Object.keys(files).forEach((i) => {
            const file = files[i];
            let url = URL.createObjectURL(file);

            arrayImages.push({
                index: indexInicial,
                url, file
            });
            indexInicial++;
        });
        return arrayImages;
    }

    const deleteImage = (indexImage) => {
        const newImgs = []
        files.map((element, index) => {
            if (index !== indexImage) {
                newImgs.push(element)
            }
        });
        if (newImgs.length === 0) {
            setConfirmation(false);
            closeFloatingMenu(false);
        }
        else
            setFiles(newImgs);
    }

    return (
        <div className='floatingMenuContainer'>
            <div className='floatingMenu' title=''>
                <div className='menuElement' >
                    <label title='Cargar Imagen'>
                        <FontAwesomeIcon className='menuIcon' icon={faImage} />
                        <span> Imágenes </span>
                        <input hidden type="file" accept='image/*' multiple onChange={addImage}></input>
                    </label>
                </div>

                <div className='menuElement'>
                    <label title='Cargar Video'>
                        <FontAwesomeIcon className='menuIcon' icon={faVideo} />
                        <span> Videos </span>
                        <input hidden type="file" accept='video/*' onChange={addImage} ></input>
                    </label>
                </div>
                <div className='menuElement'>
                    <label title='Cargar Audio'>
                        <FontAwesomeIcon className='menuIcon' icon={faMusic} />
                        <span> Audios </span>
                        <input hidden type="file" accept='audio/*' onChange={addImage} ></input>
                    </label>
                </div>
            </div>
            {confirmation ?
                <div className='confirmation'>
                    <div className='headerConfirmation'>
                        <p>Seleccionados</p>
                        <button onClick={close} title="Cerrar" className="menu__close__x">
                            <FontAwesomeIcon icon={faXmark} />
                        </button>
                    </div>
                    <div className="imagesConfirmerContainer">
                        {files.map((file, index) =>
                            <div key={index} >
                                {file.file.type == "image/jpeg" || file.file.type == "image/x-icon" || file.file.type == "image/png" ?
                                    <>
                                        <button title="Eliminar" onClick={deleteImage.bind(this, index)} className="modal_close_x">
                                            <FontAwesomeIcon icon={faXmark} />
                                        </button>
                                        <img src={file.url} className="imageconfirmer" />
                                    </>
                                    :
                                    file.file.type == "video/mp4" ?
                                        <video src={file.url} className="videoconfirmer" ></video>
                                        :
                                        file.file.type == "audio/mpeg" ?
                                            <audio src={file.url} className="audioconfirmer" preload="none" controls></audio>
                                            :
                                            null
                                }
                            </div>
                        )}
                    </div>

                    <button title='Enviar' onClick={confirmationFunc} className='sendButton'>
                        {spinner ?
                            <>Enviando...</>
                            :
                            <>Enviar</>
                        }
                    </button>
                </div>
                :
                null
            }
        </div>
    )
}