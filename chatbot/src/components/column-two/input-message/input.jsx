import { faMicrophone, faPaperPlane, faPaperclip, faStopwatch, faCheck, faXmark, faStop, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TemporaryMessageMenu } from './temporary-message/menu';
import { CircleButton } from './circle-button/circle-button';
import { updateMessage } from '../../../services/update';
import { FloatingMenu } from './upload-image-menu/menu';
import { setAudio } from '../../../services/set';
import { Spinner } from '../../spinner/spinner';
import { useEffect } from 'react';
import { useState } from 'react';
import vmsg from "vmsg";
import './styles.css';

const recorder = new vmsg.Recorder({
    wasmURL: "https://unpkg.com/vmsg@0.3.0/vmsg.wasm"
});

export const InputMessage = ({ chat, userLogged, user, idChat, message, confirmEdit, setConfirmEdit, setMessage, setFloatingMenu }) => {

    const [empty, setEmpty] = useState(true);
    const [uploadImageMenu, setUploadImageMenu] = useState(false);
    const [recording, setRecording] = useState(false);
    const [record, setRecord] = useState()
    const [recordURL, setRecordURL] = useState()
    const [recordingLoanding, setRecordingLoanding] = useState(false);
    const [autoDelete, setAutoDelete] = useState(false);
    const [dateTimeDelete, setDateTimeDelete] = useState('');

    useEffect(() => {
        Empty();
    }, [confirmEdit])

    useEffect(() => {
        if (dateTimeDelete != '')
            AutoDelete();
    }, [dateTimeDelete])

    const Empty = (e) => {
        if (document.getElementById("input").textContent === '')
            setEmpty(true);
        else
            setEmpty(false);
            setUploadImageMenu(false);
            setAutoDelete(false);
    }

    const closeFloatingMenu = () => {
        setTimeout(function () {
            setUploadImageMenu(false);
            setAutoDelete(false);
        }, 500);
    }

    const Update = () => {
        if (message !== null) {
            updateMessage(message.id, document.getElementById("input").textContent)
            document.getElementById("input").textContent = "";
            setConfirmEdit(false);
        }
    }

    const CancelUpdate = () => {
        setMessage(null);
        document.getElementById("input").textContent = '';
        setConfirmEdit(false);
    }

    const recordingFunction = async () => {
        setRecording(true);
        try {
            await recorder.initAudio();
            await recorder.initWorker();
            recorder.startRecording();
        } catch (e) {
            console.error(e);
        }
    }

    const stopRecording = async () => {
        const blob = await recorder.stopRecording();
        setRecordURL(URL.createObjectURL(blob));
        setRecord(blob);
        setRecording(false);
    }

    const cancelRecord = () => {
        setRecord(null);
        setRecordURL(null);
    }

    const sendRecord = () => {
        setRecordingLoanding(true);
        setAudio([record], chat, idChat, userLogged, "audio", setRecordingLoanding, recordURL);
        setRecord(null);
        setRecordURL(null);
    }

    const AutoDelete = () => {
        console.log(dateTimeDelete);
    }

    const openUpdate = () => {
        setUploadImageMenu(!uploadImageMenu);
        setAutoDelete(false);
    }

     const openTemporyMessage = () => {
        setAutoDelete(!autoDelete)
        setUploadImageMenu(false);
     }

    return (
        <div className='InputMessage' onClick={()=>setFloatingMenu(false)}>
            {record == null && !recording ?
                <>
                    <div className='uploadImage' title='Enviar Imagen o Video'>
                        <FontAwesomeIcon onClickCapture={openUpdate} id='icon' icon={faPaperclip} />
                        {uploadImageMenu ?
                            <div className='uploadImageMenu'><FloatingMenu chat={chat} userLogged={userLogged} user={user} idChat={idChat} closeFloatingMenu={closeFloatingMenu} /></div>
                            : null
                        }
                    </div>

                    <div onInput={Empty} id='input' className='input' contentEditable="true" placeholder="Escribe un mensaje" />

                    <div className='temporaryMessage' title='Programar duración'>
                        {dateTimeDelete != '' && !autoDelete ?
                            <FontAwesomeIcon onClickCapture={openTemporyMessage} id='iconGreen' icon={faStopwatch} />
                            :
                            <FontAwesomeIcon onClickCapture={openTemporyMessage} id='icon' icon={faStopwatch} />
                        }
                        {autoDelete ?
                            <div className='temporaryMessageMenu'><TemporaryMessageMenu setAutoDelete={setAutoDelete} setDateTimeDelete={setDateTimeDelete} AutoDelete={AutoDelete} dateTimeDelete={dateTimeDelete} autoDelete={autoDelete} /></div>
                            : null
                        }
                    </div>
                    <span className='corner' />

                    {recordingLoanding ?
                        < div className='spinnernewRecord'>
                            <Spinner />
                        </div> : null
                    }
                </>
                : null
            }

            {empty ?
                recording ?
                    <>
                        <div className='circleButtonStop' onClick={stopRecording}>
                            <FontAwesomeIcon id='icon' icon={faStop} />
                        </div>
                    </>
                    :
                    record != null ?
                        <>
                            <div className='newRecord'>
                                <audio src={recordURL} className="audioMessage" preload="none" controls></audio>
                            </div>
                            <div className='circleButton' id='audioButton'>
                                <FontAwesomeIcon id='icon' icon={faPaperPlane} onClick={sendRecord} />
                            </div>
                            <div className='circleButtonCancel' onClick={cancelRecord}>
                                <FontAwesomeIcon id='icon' icon={faTrash} />
                            </div>
                        </>
                        :
                        <div className='circleButton' onClick={recordingFunction}>
                            <FontAwesomeIcon id='icon' icon={faMicrophone} />
                        </div>

                :
                confirmEdit ?
                    <>
                        <div className='circleButton' onClick={Update}>
                            <FontAwesomeIcon id='icon' icon={faCheck} />
                        </div>
                        <div className='circleButtonCancel' onClick={CancelUpdate}>
                            <FontAwesomeIcon id='icon' icon={faXmark} />
                        </div>
                    </>
                    :
                    <CircleButton chat={chat} icon={faPaperPlane} setEmpty={setEmpty} user={user} autoDelete={dateTimeDelete} setDateTimeDelete={setDateTimeDelete} setAutoDelete={setAutoDelete} />
            }
        </div >
    )
}
