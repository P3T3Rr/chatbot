import { faXmark, faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FloatingMenu } from './input-search/floating-menu/menu';
import { ProfilePhoto } from '../profile-photo/photo';
import defaultUserImage from '../../assets/user.jpg';
import { Input } from './input-search/input';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './styles.css';

export const HeaderChat = ({ user, chat, blocked, messages, setMessagesFilter, userLogged, floatingMenu, setFloatingMenu }) => {

    const [Iblocked, setIBlocked] = useState(false);

    useEffect(() => {
        functionGetUser();
    }, [chat])

    const closeFloatingMenu = () => {
        setTimeout(function () {
            setFloatingMenu(false);
        }, 500);
    }

    const goToChats = () => {
        const display = document.getElementById("column1");
        display.style.display = 'block';
        setFloatingMenu(false);
    }

    const functionGetUser = () => {
        setIBlocked(false);
        if (userLogged.blockeds.length > 0) {
            userLogged.blockeds.map(phone => {
                if (phone === user.phone) {
                    setIBlocked(true);
                }
            })
        }
    }

    return (
        <div className='headerChat'>
            <div className='ProfilePhoto' onClick={() => setFloatingMenu(false)}>
                {!blocked ?
                    <ProfilePhoto photo={user.image} />
                    :
                    <ProfilePhoto photo={defaultUserImage} />
                }
            </div>
            <div className='texts' style={{ textAlign: "justify" }}>
                <p className='mediumText'>{user.name}</p>
                {Iblocked ?
                    <p className='smallText2' id='blocked'>Usuario Bloqueado</p>
                    :
                    <p className='smallText2'>{user.phone}</p>
                }
            </div>
            <div className='seachButton' title='Buscar' onClick={() => setFloatingMenu(false)}>
                <Input setMessagesFilter={setMessagesFilter} messages={messages} />
            </div>
            <button className='menuButton' onClick={() => setFloatingMenu(!floatingMenu)}>
                <FontAwesomeIcon icon={faEllipsisVertical} />
            </button>
            {floatingMenu ?
                <div className='chatFloatingMenu' onMouseLeave={closeFloatingMenu}>
                    <FloatingMenu user={user} chat={chat} userLogged={userLogged} setFloatingMenu={setFloatingMenu} goToChats={goToChats} blocked={Iblocked} setBlocked={setIBlocked} />
                </div>
                :
                null
            }

            <Link className='closeButton' to={"/"}>
                <div title='Cerrar'>
                    {window.screen.width > 975 ?
                        <FontAwesomeIcon icon={faXmark} />
                        :
                        <FontAwesomeIcon icon={faXmark} onClick={goToChats} />
                    }
                </div>
            </Link>
        </div>
    )
}