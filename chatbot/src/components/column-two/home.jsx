import Logo from '../../assets/logo.png';
import './styles.css';

export const Principal = () => (
    <div className='chatContainerHome'>
        <div>
            <img className='logo' src={Logo} />
        </div>
        <h2>ChatBot</h2>
        <p>La aplicación que te permite chatear de manera anónima</p>
    </div>
)