import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { updateBlockeds } from '../../../../services/update';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { Spinner } from '../../../spinner/spinner';
import { useParams } from 'react-router-dom';
import { useState } from 'react';
import './styles.css';

export const Modal = ({ chat, user, userLogged, setOpen, blocked, setFloatingMenu }) => {

    const urlphone = useParams().phone;
    const [spinner, setSpinner] = useState(false);

    const unlock = () => {
        setSpinner(true);
        const blockList = userLogged.blockeds;
        const filteredblockList = blockList.filter((item) => item !== urlphone)
        updateBlockeds(userLogged.id, filteredblockList, setSpinner, chat, userLogged.phone);
        setOpen(false);
    }

    const lock = () => {
        setSpinner(true);
        const blockList = userLogged.blockeds;
        blockList.push(urlphone);
        updateBlockeds(userLogged.id, blockList, setSpinner, chat, userLogged.phone);
        setOpen(false);
    }

    const close = () => {
        setOpen(false);
        setFloatingMenu(false);
    }

    return (
        <div className='modal' id='modalBlocked'>
            <div className="modal__container" id='modalBlockedContainer'>
                <button onClick={close} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <h2>Confirmación</h2>
                <div>
                    {blocked ?
                        <p>¿Deseas desbloquear a {user.name}?</p>
                        :
                        <p>¿Deseas bloquear a {user.name}?</p>
                    }
                </div>
                {spinner ?
                    <div id='spinner'><Spinner /></div>
                    : null
                }
                <div className='modalBlockedButtons'>
                    <button onClick={close} className='cancel' title='Cancelar'>Cancelar</button>
                    {blocked ?
                        <button onClick={unlock} className='save' title='Desbloquear'>Desbloquear</button>
                        :
                        <button onClick={lock} className='save' title='Bloquear'>Bloquear</button>
                    }
                </div>
            </div>
        </div>
    )
}