import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { deleteChat } from '../../../../services/delete';
import { useNavigate } from 'react-router-dom';

export const ModalDelete = ({ setOpen, goToChats, chat }) => {

    const navigate = useNavigate();

    const DeleteChat = () => {
        deleteChat(chat.id);
        navigate("/");
        goToChats();
    }

    return (
        <div className='modal' id='modalDelete'>
            <div className="modal__container" id='modalDeleteContainer'>
                <button onClick={()=>setOpen(false)} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <h2>Confirmación</h2>
                <div className='messageConfirmer_deleteMessage'>
                    ¿Eliminar chat completamente?
                </div>
                <div className='modalDeleteButtons'>
                    <button onClick={()=>setOpen(false)} className='cancel' title='Cancelar'>Cancelar</button>
                    <button onClick={DeleteChat} className='save' title='Eliminar Mensaje'>Eliminar</button>
                </div>
            </div>
        </div>
    )
}
