import { faUser, faUserSlash, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ModalDelete } from './modal-delete';
import { useState } from 'react';
import { Modal } from './modal';
import './styles.css';

export const FloatingMenu = ({ user, chat, userLogged, setFloatingMenu, goToChats, blocked, setBlocked }) => {
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);

    const openBlockedModal = () => {
        setOpen(true);
    }

    return (
        <div className='floatingMenuChat'>
            <div className='chatMenuElement'>
                <div title='Bloquear Usuario' onClickCapture={openBlockedModal}>
                    {blocked ?
                        <>
                            <FontAwesomeIcon className='menuIcon' icon={faUser} />
                            <span> Desbloquear Usuario </span>
                        </>
                        :
                        <>
                            <FontAwesomeIcon className='menuIcon' icon={faUserSlash} />
                            <span> Bloquear Usuario </span>
                        </>
                    }
                </div>
            </div>

            <div className='chatMenuElement'>
                <div onClick={()=>setOpenDelete(true)} title='Eliminar Conversación' id='deleteElement'>
                    <FontAwesomeIcon className='menuIcon' icon={faTrash} />
                    <span> Eliminar Conversación </span>
                </div>
            </div>

            {open ?
                <Modal setOpen={setOpen} chat={chat} user={user} userLogged={userLogged} blocked={blocked} setBlocked={setBlocked} setFloatingMenu={setFloatingMenu} />
                : null
            }

            {openDelete ?
                <ModalDelete setOpen={setOpenDelete} goToChats={goToChats} chat={chat} />
                : null
            }

        </div>
    )
}