import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import './styles.css';

export const Input = ({ messages, setMessagesFilter }) => {

    const filter = () => {
        let newList = [];
        let wordFilter = document.getElementById("searchInput").value

        if (wordFilter === '') {
            setMessagesFilter(messages);
        }
        else {
            if (messages !== null) {
                for (let index = 0; index < messages.length; index++) {
                    let mess = messages[index].message.toLowerCase()
                    let time = messages[index].time.toLowerCase()
                    let date = messages[index].date.toLowerCase()
                    let month = messages[index].month.toLowerCase()
                    let type = messages[index].type.toLowerCase()
                    let i = mess.indexOf(wordFilter.toLowerCase())
                    let j = time.indexOf(wordFilter.toLowerCase())
                    let m = date.indexOf(wordFilter.toLowerCase())
                    let n = month.indexOf(wordFilter.toLowerCase())
                    let t = type.indexOf(wordFilter.toLowerCase())
                    if (i >= 0 || j >= 0 || m >= 0 || n >= 0 || t >= 0) {
                        newList.push(messages[index]);
                    }
                }
                if (newList.length === 0)
                    setMessagesFilter(null);
                else
                    setMessagesFilter(newList);
            }
        }
    }

    return (
        <div className='inputContainerChat'>
            <form role="search" method="get" id="searchform" action="">
                <label htmlFor="searchInput">
                    <FontAwesomeIcon id='icon' icon={faSearch} />
                </label>
                <input type="text" onChange={filter} placeholder="Buscar mensaje" id="searchInput" />
                <div className='span'></div>
            </form>
        </div>
    )
}