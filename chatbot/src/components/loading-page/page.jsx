import { Spinner } from '../spinner/spinner';
import Logo from '../../assets/logo.png';
import './styles.css';

export const LoadingPage = () => {

    return (
        <div className="loadingPage">
            <div>
                <img src={Logo} />
            </div>
            <div className='spinner'>
                <Spinner />
            </div>
            <h2>ChatBot by JJK</h2>
        </div>
    )
}