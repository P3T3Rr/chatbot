import { faTriangleExclamation, faUpload, faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Spinner } from '../../spinner/spinner';
import { useState } from 'react';
import './styles.css'
import { updateUser } from '../../../services/update';

export const User = ({ setUser, user }) => {

    const [alert, setAlert] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [imageURL, setImageURL] = useState(user.image);
    const [image, setImage] = useState(user.image);

    const update = () => {
        const name = document.getElementById("userName").value;
        const mail = document.getElementById("userMail").value;
        const password = document.getElementById("userPassword").value;
        if (name === "" || mail === "" || password === "") {
            setAlert(true);
        }
        else {
            setSpinner(true);
            updateUser(user.id, user.phone, name, mail, image, password, setSpinner, setUser)
        }
    }

    const addImage = (e) => {
        const file = e.currentTarget.files[0];
        let url = URL.createObjectURL(file);
        setImageURL(url)
        setImage(file)
    }

    return (
        <div className='modal'>
            <div className="modal__container">
                <button onClick={() => setUser(false)} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <h2>Gestionar Cuenta</h2>

                <div>
                    <div className='profilePhotoContainerUser'>
                        <img src={imageURL} />
                    </div>
                    <div className='upload_image'>
                        <label className='upload_button' title='Cargar Imagen'>
                            <FontAwesomeIcon icon={faUpload} id="uploadIcon" />
                            <input hidden type="file" accept='image/*' onChange={addImage}></input>
                        </label>
                    </div>
                </div>

                <div className='userPhone'>
                    <p>{user.phone}</p>
                </div>

                <div className='userDate'>
                    <p>Unido desde: {user.date}</p>
                </div>

                <form action="">
                    <input type="text" id='userName' placeholder='Nombre' defaultValue={user.name} />
                    <input type="text" id='userMail' placeholder='Correo electrónico' defaultValue={user.email} />
                    <input type="password" id='userPassword' placeholder='Contraseña' defaultValue={user.password} />
                </form>

                {spinner ?
                    <div className='spinnerUser'><Spinner /></div>
                    : null
                }

                {alert ?
                    <div><p className='alertMessage' id='alertUser'><FontAwesomeIcon id='iconAlert' icon={faTriangleExclamation} />Campos Vacíos</p></div>
                    :
                    null
                }

                <div>
                    <button onClick={() => setUser(false)} className='cancel' title='Cancelar'>Cancelar</button>
                    <button onClick={update} className='userButton' title='Acualizar'>Actualizar</button>
                </div>
            </div>
        </div>
    )
}
