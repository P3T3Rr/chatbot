import { IndividualChat } from './individual-chat';
import './styles.css';

export const Chats = ({ chats, userFloatingMenu, setUserFloatingMenu  }) => (
    <div className="menuChats">
        {chats.length == 0 ?
            <h1>No hay chats</h1>
            :
            chats.map((element, index) =>
                <div key={index}>
                    <IndividualChat chat={element} chats={chats} userFloatingMenu={userFloatingMenu} setUserFloatingMenu={setUserFloatingMenu} />
                </div>
            )
        }
    </div>
)