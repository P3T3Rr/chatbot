import { faRobot, faPhotoFilm, faMicrophone, faMusic } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ProfilePhoto } from '../../profile-photo/photo';
import defaultUserImage from '../../../assets/user.jpg';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import './styles.css';

export const IndividualChat = ({ chat, chats, setUserFloatingMenu }) => {
    const init = () => {
        const UserLogged = localStorage.getItem("user");
        return UserLogged ? JSON.parse(UserLogged) : [];
    }

    const UserLogged = init();
    const [user, setUser] = useState();
    const [blocked, setBlocked] = useState(false);
    const [newMessages, setNewMessages] = useState(0);

    useEffect(() => {
        functionGetUser();
        getNewMessage();
    }, [])

    useEffect(() => {
        functionGetUser();
        getNewMessage();
    }, [chats])

    const getNewMessage = () => {
        if (chat.news.lengt != 0) {
            let news = 0
            chat.news.map(newMessage => {
                console.log(chat.news[0]);
                if (newMessage.user !== UserLogged) {
                    news += 1
                }
            })
            setNewMessages(news);
        }
    }

    const functionGetUser = () => {
        setBlocked(false);
        let userInfo = { "name": chat.userClientName, "phone": chat.userClientPhone, "image": chat.userClientImage, "email": chat.userClientEmail, "blockeds": chat.userClientBlockeds };

        if (userInfo.blockeds !== []) {
            userInfo.blockeds.map(phone => {
                if (phone == UserLogged) {
                    setBlocked(true);
                }
            })
        }
        setUser(userInfo)
    }

    const goToChats = () => {
        const display = document.getElementById("column1");
        display.style.display = 'none';
        setUserFloatingMenu(false);
    }

    return (
        <div className='menuChat'>
            {user != null ?
                <>
                    <div className='photoContainer'>
                        {!blocked ?
                            <ProfilePhoto photo={user.image} />
                            :
                            <ProfilePhoto photo={defaultUserImage} />
                        }
                    </div>
                    <div className='texts'>
                        {window.screen.width > 975 ?
                            <NavLink onClick={() => setUserFloatingMenu(false)} className='link' to={"/" + chat.id + "/" + user.phone}>
                                <p className='mediumText'>{user.name}</p>
                                <p className='smallText'>
                                    {chat.message === 'Imagen' ?
                                        <FontAwesomeIcon icon={faRobot} />
                                        :
                                        chat.message === "Archivo multimedia" ?
                                            <FontAwesomeIcon icon={faPhotoFilm} />
                                            :
                                            chat.message === "Musica" ?
                                                <FontAwesomeIcon icon={faMusic} />
                                                :
                                                chat.message === "Audio" ?
                                                    <FontAwesomeIcon icon={faMicrophone} />
                                                    :
                                                    chat.message === "Clima" ?
                                                        <FontAwesomeIcon icon={faRobot} />
                                                        :
                                                        null
                                    } {chat.message}
                                </p>
                            </NavLink>
                            :
                            <NavLink onClick={goToChats} className='link' to={"/" + chat.id + "/" + user.phone}>
                                <p className='mediumText'>{user.name}</p>
                                <p className='smallText'>
                                    {chat.message === 'Imagen' ?
                                        <FontAwesomeIcon icon={faRobot} />
                                        :
                                        chat.message === "Archivo multimedia" ?
                                            <FontAwesomeIcon icon={faPhotoFilm} />
                                            :
                                            chat.message === "Musica" ?
                                                <FontAwesomeIcon icon={faMusic} />
                                                :
                                                chat.message === "Audio" ?
                                                    <FontAwesomeIcon icon={faMicrophone} />
                                                    :
                                                    chat.message === "Clima" ?
                                                        <FontAwesomeIcon icon={faRobot} />
                                                        :
                                                        null
                                    } {chat.message}
                                </p>
                            </NavLink>
                        }
                    </div>
                    <div className='dateChats'>
                        {newMessages > 0 ?
                            <div className='newMessagesNumber'><span>{newMessages}</span></div>
                            :
                            null
                        }
                        <p>{chat.hour}</p>
                    </div>
                </> : null
            }
        </div>
    )
}