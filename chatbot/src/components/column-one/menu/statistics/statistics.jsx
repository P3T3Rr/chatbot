import { getStadistics } from '../../../../stadistics/getStadistics';
import { setinteractions } from '../../../../stadistics/setInteractions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { useEffect, useState } from 'react';
import './styles.css';

export const Statistics = ({ setModal, setUserFloatingMenu, stadistics, userId }) => {

    const init = () => {
        const UserLogged = localStorage.getItem("user");
        return UserLogged ? JSON.parse(UserLogged) : [];
    }

    const UserLogged = init();

    const [statisticsJson, setStatistics] = useState({
        messages: "",
        images: "",
        videos: "",
        audios: "",
        words: "",
        moreInteraction: "",
        lessInteraction: ""
    });

    useEffect(() => {
        setinteractions(UserLogged, userId);

        const data = getStadistics(stadistics);
        data.then(function (statistic) {
            console.log(statistic);
            setStatistics(statistic);
        });
    }, [])

    const close = () => {
        setModal(false)
        setUserFloatingMenu(false);
    }

    return (
        <div className='modal' id='modalStatistics'>
            <div className="modal__container" id='modalStatisticsContainer'>
                <button onClick={close} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <div>
                    <h2>Estadísticas</h2>
                </div>

                <table>
                    <tbody>
                        <tr>
                            <td>Mensajes enviados:</td>
                            <td id='right'>{statisticsJson.messages}</td>
                        </tr>
                        <tr>
                            <td>Imágenes enviadas:</td>
                            <td id='right'>{statisticsJson.images}</td>
                        </tr>
                        <tr>
                            <td>Videos enviados:</td>
                            <td id='right'>{statisticsJson.videos}</td>
                        </tr>
                        <tr>
                            <td>Audios enviados:</td>
                            <td id='right'>{statisticsJson.audios}</td>
                        </tr>
                        <tr>
                            <td>Palabras enviados:</td>
                            <td id='right'>{statisticsJson.words}</td>
                        </tr>
                        <tr>
                            <td>Mayor interacción:</td>
                            <td id='right'>{statisticsJson.moreInteraction}</td>
                        </tr>
                        <tr>
                            <td>Menor interacción:</td>
                            <td id='right'>{statisticsJson.lessInteraction}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}