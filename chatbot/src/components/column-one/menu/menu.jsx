import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FloatingMenu } from './user-menu/menu';
import './styles.css';

export const Menu = ({ setLogged, user, userFloatingMenu, setUserFloatingMenu }) => {

    const closeFloatingMenu = () => {
        setTimeout(function () {
            setUserFloatingMenu(false);
        }, 500);
    }

    return (
        <>
            <button className='sandwichMenu' onClick={() => setUserFloatingMenu(!userFloatingMenu)}>
                <FontAwesomeIcon icon={faBars} />
            </button>
            {userFloatingMenu ?
                <div className='userFloatingMenu' onMouseLeave={closeFloatingMenu} ><FloatingMenu setLogged={setLogged} setUserFloatingMenu={setUserFloatingMenu} user={user} /></div>
                : null
            }
        </>
    )
}