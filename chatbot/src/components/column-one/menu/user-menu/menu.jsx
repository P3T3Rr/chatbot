import { faUser, faChartLine, faRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Statistics } from '../statistics/statistics';
import { useState } from 'react';
import './styles.css';
import { User } from '../../user/user';

export const FloatingMenu = ({ setLogged, setUserFloatingMenu, user }) => {

    const [modal, setModal] = useState(false);
    const [userModal, setUser] = useState(false);

    const logout = () => {
        localStorage.setItem("user", JSON.stringify(""));
        setLogged(false);
    }

    const openStatistics = () => {
        setModal(true);
    }

    const openUser = () => {
        setUser(true);
    }

    return (
        <div className='floatingMenuUser'>
            <div className='userMenuElement' onClick={openUser}>
                <div title='Cuenta'>
                    <FontAwesomeIcon className='menuIcon' icon={faUser} />
                    <span> Cuenta </span>
                </div>
            </div>

            <div className='userMenuElement' onClick={openStatistics}>
                <div title='Estadísticas'>
                    <FontAwesomeIcon className='menuIcon' icon={faChartLine} />
                    <span> Estadísticas </span>
                </div>
            </div>

            <div className='userMenuElement'>
                <div title='Cerrar Sesión' id='logout' onClick={logout}>
                    <FontAwesomeIcon className='menuIcon' icon={faRightFromBracket} />
                    <span> Cerrar Sesión </span>
                </div>
            </div>

            {modal ?
                <Statistics setModal={setModal} setUserFloatingMenu={setUserFloatingMenu} stadistics={user.stadistics} userId={user.id} />
                :
                null
            }

            {userModal ?
                <User setUser={setUser} user={user} />
                :
                null
            }
        </div>
    )
}