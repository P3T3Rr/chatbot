import { Button } from './new-conversation-button/button';
import { ProfilePhoto } from '../profile-photo/photo';
import { SearchChat } from './search-chat/search';
import { useEffect, useState } from 'react';
import { Chats } from './menu-chats/chats';
import { Menu } from './menu/menu';
import './styles.css';

export const ColumnOne = ({ user, chats, setLogged }) => {

    const [filterChats, setFilterChats] = useState(chats);
    const [userFloatingMenu, setUserFloatingMenu] = useState(false);

    useEffect(() => {
        setFilterChats(chats);
    }, [chats]);

    
    const goToChats = () => {
        window.location.hash = "no-back-button";
        window.location.hash = "Again-No-back-button"
        window.onhashchange = function () {
            window.location.hash = "no-back-button";
        }
    }

    return (
        <div className='column1' id='column1' onLoad={goToChats} >
            {user != null ?
                <div className='columnHeader1'>
                    <div className='ProfilePhoto' title={user.name}>
                        <ProfilePhoto photo={user.image} />
                    </div>
                    <div className='inputContainer'>
                        <SearchChat setFilterChats={setFilterChats} original={chats} />
                    </div>
                    <div className='menuContainer'>
                        <Menu setLogged={setLogged} user={user} userFloatingMenu={userFloatingMenu} setUserFloatingMenu={setUserFloatingMenu} />
                    </div>
                </div>
                :
                null
            }
            <Chats chats={filterChats} userFloatingMenu={userFloatingMenu} setUserFloatingMenu={setUserFloatingMenu} />
            <Button userLogged={user} />
        </div>
    )
}
