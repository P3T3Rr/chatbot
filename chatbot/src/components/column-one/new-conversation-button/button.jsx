import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComments } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import { Modal } from './modal';
import './styles.css';

export const Button = ({ userLogged }) => {
    const [open, setOpen] = useState(false);

    return (
        <div className='newConversation' title='Nuevo Chat'>
            <button onClick={() => setOpen(true)}>
                <FontAwesomeIcon id='icon' icon={faComments} />
            </button>
            {open ?
                <Modal setOpen={setOpen} userLogged={userLogged} />
                : null
            }
        </div>
    )
}