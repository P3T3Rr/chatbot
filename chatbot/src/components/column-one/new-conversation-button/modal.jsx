import { faXmark, faTriangleExclamation } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { firebase } from '../../../services/firebaseConfig';
import { getChat, getUser } from '../../../services/get';
import { Encrypt } from '../../../services/crypto-js';
import CR from '../../../assets/contries/cr.png';
import { setChat } from '../../../services/set';
import { Spinner } from '../../spinner/spinner';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import './styles.css';

export const Modal = ({ setOpen, userLogged }) => {

    const navigate = useNavigate();
    const [alertMessage, setAlertMessage] = useState("");
    const [alert, setAlert] = useState(false);
    const [spinner, setSpinner] = useState(false);

    const save = () => {
        const number = "+506 " + document.getElementById("newChatnumber").value;

        if (number === "+506 ") {
            setAlertMessage("Digite un número");
            setAlert(true);
        }
        else {
            if (number !== userLogged.phone) {
                setSpinner(true);
                let data = getUser(number);
                data.then(function (user) {
                    if (user != undefined) {
                        const date = firebase.firestore.Timestamp.now().toDate()
                        const newChat = {
                            "message": Encrypt('Sin mensajes'),
                            "date": date,
                            "nameUser1": Encrypt(userLogged.name), "nameUser2": Encrypt(user.name),
                            "phoneUser1": userLogged.phone, "phoneUser2": user.phone,
                            "imageUser1": Encrypt(userLogged.image), "imageUser2": Encrypt(user.image),
                            "emailUser1": Encrypt(userLogged.email), "emailUser2": Encrypt(user.email),
                            "blockedsUser1": userLogged.blockeds, "blockedsUser2": user.blockeds,
                            "news": []
                        }

                        let chat = getChat(setSpinner, userLogged.phone, number);
                        chat.then(function (idChat) {
                            if (idChat !== undefined) {
                                setOpen(false);
                                navigate("/" + idChat + "/" + user.phone);
                            }
                            else {
                                let newId = setChat(newChat, setSpinner);
                                newId.then(function (id) {
                                    setOpen(false);
                                    navigate("/" + id + "/" + user.phone);
                                })
                            }

                            if (window.screen.width < 975) {
                                const display = document.getElementById("column1");
                                display.style.display = 'none';
                            }
                        })

                    }
                    else {
                        setAlertMessage("Ese número no se encuentra registrado");
                        setAlert(true);
                    }
                    setSpinner(false);
                })
            }
            else {
                setAlertMessage("Use un número distinto al suyo");
                setAlert(true);
            }
        }

    }

    const close = () => {
        setOpen(false);
    }

    return (
        <div className='modal'>
            <div className="modal__container">
                <button onClick={close} title="Cerrar" className="modal__close__x">
                    <FontAwesomeIcon icon={faXmark} />
                </button>
                <h2>Nueva Conversación</h2>
                <div className='numberContainer'>
                    <img src={CR} />
                    <p>+506</p>
                    <input type="text" id='newChatnumber' placeholder='Número de teléfono' />
                </div>
                {spinner ?
                    <div id='spinner'><Spinner /></div>
                    : null
                }
                {alert ?
                    <div><p className='alertMessage'><FontAwesomeIcon id='iconAlert' icon={faTriangleExclamation} />{alertMessage}</p></div>
                    : null
                }
                <div>
                    <button onClick={close} className='cancel' title='Cancelar'>Cancelar</button>
                    <button onClick={save} className='save' title='Nueva Conversación'>Crear Chat</button>
                </div>
            </div>
        </div>
    )
}