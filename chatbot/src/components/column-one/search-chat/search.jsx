import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import './styles.css';

export const SearchChat = ({ original, setFilterChats }) => {

    const filter = () => {
        let newList = [];
        let wordFilter = document.getElementById("inputSearchChats").value

        if (wordFilter === '') {
            setFilterChats(original);
        }
        else {
            for (let index = 0; index < original.length; index++) {
                let name = original[index].userClientName.toLowerCase()
                let hour = original[index].hour.toLowerCase()
                let i = name.indexOf(wordFilter.toLowerCase())
                let h = hour.indexOf(wordFilter.toLowerCase())
                if (i >= 0 || h >= 0) {
                    newList.push(original[index]);
                }
            }
            setFilterChats(newList);
        }
    }

    return (
        <form onSubmit={filter}>
            <label className='searchChat'>
                <FontAwesomeIcon id='icon' icon={faSearch} />
                <input onChange={filter} id='inputSearchChats' type="text" placeholder='Buscar' />
            </label>
        </form>
    )
}