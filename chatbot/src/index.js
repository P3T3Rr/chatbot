import { BrowserRouter } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import ReactDOM from 'react-dom/client';
import React from 'react';
import App from './App';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

// const helloWord = () => {
//   console.log("-----------------------------");
//   console.log("----------Hello Word---------");
//   console.log("-----------------------------");
// }

root.render(
  <BrowserRouter>
    <App/>
  </BrowserRouter>
);

reportWebVitals();
