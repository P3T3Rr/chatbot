export const getDate = () => {
    let date = new Date();
    let year = date.getFullYear();
    let day = date.getDate();
    let month = date.getMonth();

    month = month + 1
    if (month < 10) {
        month = "0" + month;
    } else {
        month = month.toString;
    }

    date = year + "-" + month + "-" + day;
    return date;
}