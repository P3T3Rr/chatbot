import { NewMessage, setImageBot } from "../services/set";
import { getWeather } from "../apis/openWeather";
import { serchImage } from "../apis/serchImage";
import { getStadistics } from "../services/get";
import { updateStadistics } from "../services/update";

export const Send = (idChat, chat, urlphone, user, setEmpty, autoDelete) => {
    const message = document.getElementById("input").innerText;
    if (message.trim().length > 0) {
        const arr = message.split(' ');
        if (arr[0] === "@image") {
            try {
                const filter = message.split('@image ');
                let images = serchImage(filter[1]);
                images.then(function (result) {
                    setEmpty(true);
                    document.getElementById('input').textContent = "";
                    setImageBot(result, chat, idChat, urlphone)
                })
            } catch (error) {
                console.log(error);
            }

        }
        else if (message === "@weather") {
            try {
                let weatherData = getWeather(1);
                weatherData.then(function (result) {
                    setEmpty(true);
                    document.getElementById('input').textContent = "";
                    if (result !== "")
                        NewMessage(idChat, chat, result, "weather", urlphone, '');
                    else      
                        NewMessage(idChat, chat, "Error en la respuesta de la api", "weather", urlphone, '');         
                })
            } catch (error) {
                console.log(error);
            }
        }
        else {
            let stadistics = getStadistics(urlphone);
            stadistics.then(function (result) {
                let numWordsMessage = arr.length
                result[0] = result[0] + 1
                result[4] = result[4] + numWordsMessage
                setEmpty(true);
                document.getElementById('input').textContent = ""; 
                NewMessage(idChat, chat, message, "text", urlphone, "", autoDelete);
                updateStadistics(user.id, result)
            }) 
        }
    }
    else {
        setEmpty(true);
        document.getElementById('input').textContent = "";
    }
}