import { doc, deleteDoc } from "firebase/firestore";
import { db } from "./firebaseConfig";

export const deleteMessage = async (id) => {
    try {
        await deleteDoc(doc(db, 'messages', id));
    }
    catch (e) {
        console.log(e)
    }
};

export const deleteUser = async (id) => {
    try {
        await deleteDoc(doc(db, 'messages', id));
    }
    catch (e) {
        console.log(e)
    }
};

export const deleteChat = async (id) => {
    try {
        await deleteDoc(doc(db, 'chats', id));
    }
    catch (e) {
        console.log(e)
    }
};
