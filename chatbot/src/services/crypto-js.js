import CryptoJS from "crypto-js";
import key from "./keyValue";

const Encrypt = string =>{
    return CryptoJS.AES.encrypt(string,key).toString()
}

const Decrypt = string =>{
    return CryptoJS.AES.decrypt(string,key).toString(CryptoJS.enc.Utf8)
}

export {Encrypt, Decrypt}
