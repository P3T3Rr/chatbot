import { collection, doc, increment, setDoc, updateDoc } from "firebase/firestore";
import { Encrypt } from "./crypto-js";
import { db, db2, firebase } from "./firebaseConfig";
import { updateChat } from "./update";

export const NewMessage = async (idChat, chat, message, type, urlphone, autoDelete) => {
    try {
        const date = firebase.firestore.Timestamp.now().toDate()
        const encryptMessage = Encrypt(message)
        const encryptType = Encrypt(type)
        const encryptUrlphone = Encrypt(urlphone)
        const news = chat.news;
        news.push({ "user": urlphone, "message": message });
        const newMessage = { "chat": idChat, "date": date, "message": encryptMessage, "type": encryptType, "user": encryptUrlphone, "delete": autoDelete };
        await setDoc(doc(collection(db, "messages")), newMessage);
        if (type == "weather") {
            await updateDoc(doc(db, "chats", idChat), { message: Encrypt("Clima"), date: date, cantMessages: increment(1), news: news });
        } else {
            await updateDoc(doc(db, "chats", idChat), { message: encryptMessage, date: date, cantMessages: increment(1), news: news });
        }
    }
    catch (e) {
        console.log(e);
    }
}

export const NewUser = async (name, number, mail, password, image, setSpinner) => {
    try {
        const date = firebase.firestore.Timestamp.now().toDate()

        if (image != null) {
            const storageRef = firebase.storage().ref('users');
            const archivoPath = storageRef.child(image.name);
            await archivoPath.put(image)
            await archivoPath.getDownloadURL()
                .then(async (url) => {
                    newUser(name, number, mail, password, url, date)
                });
        }
        else {
            const defaultImage = 'https://firebasestorage.googleapis.com/v0/b/glass-6e942.appspot.com/o/users%2Fico.png?alt=media&token=939915e7-c3a7-4d92-bca5-1abc06385c62';
            newUser(name, number, mail, password, defaultImage, date);
        }

        setSpinner(false);
    }
    catch (e) {
        console.log(e);
    }
}

const newUser = async (name, number, mail, password, image, date) => {
    const newUser = {
        "name": name,
        "phone": number,
        "email": mail,
        "password": password,
        "blockeds": [],
        "image": image,
        "date": date,
        "stadistics": [0, 0, 0, 0, 0, "Sin Chats", "Sin Chats"]
    }
    await setDoc(doc(collection(db, "users")), newUser);
}

export const setChat = async (newChat, setSpinner) => {
    try {
        const newDoc = await db2.collection('chats').add(newChat);
        const id = newDoc.id
        setSpinner(false);
        return id
    }
    catch (error) {
        console.log(error);
    }
}

export const setFile = async (images, chat, idChat, urlphone, type, closeFloatingMenu, setSpinner, setConfirmation) => {
    console.log(images);
    try {
        const encryptUrlphone = Encrypt(urlphone)
        const encryptType = Encrypt(type)
        const encryptMessageMusic = Encrypt("Musica")
        const encryptMessageAM = Encrypt("Archivo multimedia")
        const news = chat.news;
        Object.keys(images).forEach(async i => {
            news.push({ "user": urlphone, "message": "media" });
            const storageRef = firebase.storage().ref('messages');
            const archivoPath = storageRef.child(images[i].file.name);
            await archivoPath.put(images[i].file)
            await archivoPath.getDownloadURL()
                .then(async (url) => {
                    const date = firebase.firestore.Timestamp.now().toDate()
                    const newMessage = { "chat": idChat, "date": date, "message": "", "type": encryptType, "user": encryptUrlphone, "url": url };
                    await setDoc(doc(collection(db, "messages")), newMessage);
                    if (type === "audio")
                        await updateDoc(doc(db, "chats", idChat), { message: encryptMessageMusic, date: date, news: news });
                    else
                        await updateDoc(doc(db, "chats", idChat), { message: encryptMessageAM, date: date, news: news  });
                });
            setSpinner(false)
            setConfirmation(false)
            closeFloatingMenu()
        })
    }
    catch (e) {
        console.log(e);
    }
};

export const setAudio = async (images, chat, idChat, urlphone, type, setSpinner, name) => {
    console.log(images);
    try {
        const encryptUrlphone = Encrypt(urlphone)
        const encryptType = Encrypt(type)
        const encryptMessageA = Encrypt("Audio")
        const news = chat.news;
        news.push({ "user": urlphone, "message": "image" });
        Object.keys(images).forEach(async i => {
            const storageRef = firebase.storage().ref('messages');
            const archivoPath = storageRef.child(name);
            console.log("todo gut");
            await archivoPath.put(images[i])
            await archivoPath.getDownloadURL()
                .then(async (url) => {
                    const date = firebase.firestore.Timestamp.now().toDate()
                    const newMessage = { "chat": idChat, "date": date, "message": "", "type": encryptType, "user": encryptUrlphone, "url": url };
                    await setDoc(doc(collection(db, "messages")), newMessage);
                    await updateDoc(doc(db, "chats", idChat), { message: encryptMessageA, date: date, news: news });
                });
            setSpinner(false)
        })
    }
    catch (e) {
        console.log(e);
    }
};

export const setImageBot = async (image, chat, idChat, urlphone) => {
    try {
        const date = firebase.firestore.Timestamp.now().toDate()
        const url = image[0];
        const encryptUrlphone = Encrypt(urlphone);
        const news = chat.news;
        news.push({ "user": urlphone, "message": "image" });
        const newMessage = { "chat": idChat, "date": date, "message": "", "type": Encrypt("image"), "user": encryptUrlphone, "url": url };
        updateChat(idChat, Encrypt('Imagen'), news);
        await setDoc(doc(collection(db, "messages")), newMessage);
    } catch (error) {
        console.log(error)
    }
}