import { collection, getDocs, orderBy, query, where } from "firebase/firestore";
import { createMessageAdapter } from "../adapters/message-adapter";
import { createChatsAdapter } from "../adapters/chat-adapter";
import { createUserAdapter } from "../adapters/user-adapter";
import { db } from "./firebaseConfig";

export const getUser = async (id) => {
    try {
        let user = [];
        const data = await getDocs(query(collection(db, "users"), where("phone", "==", id)));
        console.log("---------------------\nLlamo a un user: ");
        data.forEach(doc => {
            console.log(doc.data())
            user.push(createUserAdapter(doc.data(), doc.id));
        });
        return user[0];
    }
    catch (error) {
        console.log(error);
    }
}

export const getMessages = async (idChat, setSpinner) => {
    try {
        let list = [];
        const data = await getDocs(query(collection(db, 'messages'), orderBy("date")));
        console.log("-----------------------------------------------\nLlamo a los mensajes: ");
        data.forEach(doc => {
            if (doc.data().chat === idChat) {
                list.push(createMessageAdapter(doc.data(), doc.id));
                console.log(doc.data());
            }
        });
        setSpinner(false);
        return list;

    } catch (error) {
        console.log(error);
    }
}

export const getChats = async (setSpinner, logged, column) => {
    try {
        let list = [];
        const data = await getDocs(query(collection(db, 'chats'), where(column, "==", logged)));
        console.log("-----------------------------------------------\nLlamo a los chats: ");
        data.forEach(doc => {
            list.push(createChatsAdapter(doc.id, doc.data()));
            console.log(doc.data())
        });
        setSpinner(false);
        return list;

    } catch (error) {
        console.log(error);
    }
}

export const getChat = async (setSpinner, logged, otherUser) => {
    try {
        let id;
        const data = await getDocs(query(collection(db, "chats"), where("phoneUser1", "==", otherUser), where("phoneUser2", "==", logged)));
        data.forEach(doc => {
            id = (doc.id);
            console.log("Encontro el chat")
        });

        const data2 = await getDocs(query(collection(db, "chats"), where("phoneUser2", "==", otherUser), where("phoneUser1", "==", logged)));
        data2.forEach(doc => {
            id = (doc.id);
            console.log("Encontro el chat del otro lado")
        });

        setSpinner(false);
        return id;

    } catch (error) {
        console.log(error);
    }
}

export const getChatById = async (id) => {
    try {
        let list = [];
        const data = await getDocs(query(collection(db, 'chats', id)));
        console.log("-----------------------------------------------\nLlamo al chat: ");
        data.forEach(doc => {
            list.push(createChatsAdapter(doc.id, doc.data()));
            console.log(list[0]);
        });
        return list[0];

    } catch (error) {
        console.log(error);
    }
}

export const getStadistics = async (id) => {
    try {
        let stadistics = [];
        const data = await getDocs(query(collection(db, "users"), where("phone", "==", id)));
        data.forEach(doc => {
            console.log(doc.data())
            stadistics.push(doc.data().stadistics);
        });
        return stadistics[0];
    }
    catch (error) {
        console.log(error);
    }
}
