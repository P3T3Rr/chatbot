import { collection, doc, getDocs, query, updateDoc, where } from "firebase/firestore";
import { Encrypt } from "./crypto-js";
import { db, firebase } from './firebaseConfig';

export const updateMessage = async (id, message) => {
  try {
    await updateDoc(doc(db, "messages", id), {
      message: Encrypt(message),
    });
  }
  catch (error) {
    console.log(error)
  }
};

export const updateUser = async (id, phone, newName, newEmail, image, newPassword, setSpinner, setUser) => {
  try {

    try {
      const storageRef = firebase.storage().ref('users');
      const archivoPath = storageRef.child(image.name);
      await archivoPath.put(image)
      await archivoPath.getDownloadURL()
        .then(async (url) => {
          await updateDoc(doc(db, "users", id), {
            name: newName,
            email: newEmail,
            image: url,
            password: newPassword
          });

          updateChatPhoto(phone, newName, newEmail, url)
        })
    } catch {
      await updateDoc(doc(db, "users", id), {
        name: newName,
        email: newEmail,
        image: image,
        password: newPassword
      });
      updateChatPhoto(phone, newName, newEmail, image)
    }
    setSpinner(false);
    setUser(false);
  }
  catch (error) {
    console.log(error)
  }
};

const updateChatPhoto = async (phone, name, email, photo) => {
  const data = await getDocs(query(collection(db, "chats"), where("phoneUser1", "==", phone)));
  data.forEach(async docc => {
    await updateDoc(doc(db, "chats", docc.id), {
      nameUser1: Encrypt(name),
      emailUser1: Encrypt(email),
      imageUser1: Encrypt(photo)
    });
  });

  const data2 = await getDocs(query(collection(db, "chats"), where("phoneUser2", "==", phone)));
  data2.forEach(async docc => {
    await updateDoc(doc(db, "chats", docc.id), {
      nameUser2: Encrypt(name),
      emailUser2: Encrypt(email),
      imageUser2: Encrypt(photo)
    });
  });
}

export const updateChat = async (id, message, list) => {
  try {
    const date = firebase.firestore.Timestamp.now().toDate()
    await updateDoc(doc(db, "chats", id), {
      message: message,
      date: date,
      news: list
    });
  }
  catch (error) {
    console.log(error)
  }
};

export const updateBlockeds = async (idUser, blockeds, setSpinner, chat, userPhone) => {
  try {
    await updateDoc(doc(db, "users", idUser), { blockeds: blockeds });
    let collectionName = "blockedsUser1";
    const data = await getDocs(query(collection(db, 'chats')));
    data.forEach(doc => {
      if (doc.id === chat.id) {
        if (doc.data().phoneUser2 === userPhone) {
          collectionName = "blockedsUser2";
        }
      }
    });

    if (collectionName === "blockedsUser1") {
      await updateDoc(doc(db, "chats", chat.id), { blockedsUser1: blockeds });
    } else {
      await updateDoc(doc(db, "chats", chat.id), { blockedsUser2: blockeds });
    }
    setSpinner(false);
  }
  catch (error) {
    console.log(error)
  }
};

export const updateStadistics = async (id, stadistics) => {
  try {
    await updateDoc(doc(db, "users", id), {
      stadistics: stadistics
    });
  }
  catch (error) {
    console.log(error)
  }
};
