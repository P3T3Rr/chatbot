import firebase from "firebase/compat/app";
import "firebase/firestore";
import { getFirestore } from "firebase/firestore";
import 'firebase/compat/storage';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCJx80khMb02sh472ANW_lIdQPNglNuLWw",
  authDomain: "glass-6e942.firebaseapp.com",
  projectId: "glass-6e942",
  storageBucket: "glass-6e942.appspot.com",
  messagingSenderId: "1090461287941",
  appId: "1:1090461287941:web:679913fc90d765782b7e6c"
};

firebase.initializeApp(firebaseConfig);

const db2 = firebase.firestore();
const db = getFirestore();

export { db, db2, firebase }

