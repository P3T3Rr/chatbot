import { Decrypt } from "../services/crypto-js"

export const createMessageAdapter = (data,id) => {
    const date = new Date(data.date.seconds * 1000);
    const months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    const monthsWord = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    const minutes = "0" + date.getMinutes();

    return {
        "id": id,
        "chat": data.chat , 
        "dateTimestamp": date, 
        "date": String(date.getDate()+"/"+months[date.getMonth()]+"/"+date.getFullYear()),
        "time": String(date.getHours()+":"+minutes.substr(-2)),
        "month": monthsWord[date.getMonth()],
        "year": date.getFullYear(),
        "message": Decrypt(data.message),
        "type": Decrypt(data.type),
        "user": Decrypt(data.user),
        "url": String(data.url)};
}