import { Decrypt } from "../services/crypto-js";

export const createChatsAdapter = (id, chat) => {
    const init = () => {
        const UserLogged = localStorage.getItem("user");
        return UserLogged ? JSON.parse(UserLogged) : [];
    }
    const UserLogged = init();
    const date = new Date(chat.date.seconds * 1000);
    const minutes = "0" + date.getMinutes();
    const hour = String(date.getHours()+":"+minutes.substr(-2));
    if (chat.phoneUser1 === UserLogged){
        return {
            "id": id,
            "message": Decrypt(chat.message),
            "dateTimestamp": chat.date,
            "hour": hour,
            "userLoggedName": Decrypt(chat.nameUser1), "userClientName": Decrypt(chat.nameUser2),
            "userLoggedPhone": chat.phoneUser1, "userClientPhone": chat.phoneUser2,
            "userLoggedImage": Decrypt(chat.imageUser1), "userClientImage": Decrypt(chat.imageUser2),
            "userLoggedEmail": Decrypt(chat.emailUser1), "userClientEmail": Decrypt(chat.emailUser2),
            "userLoggedBlockeds": chat.blockedsUser1, "userClientBlockeds": chat.blockedsUser2,
            "news": chat.news
        };
    }
    else{
        return {
            "id": id,
            "message": Decrypt(chat.message),
            "dateTimestamp": chat.date,
            "hour": hour,
            "userLoggedName": Decrypt(chat.nameUser2), "userClientName": Decrypt(chat.nameUser1),
            "userLoggedPhone": chat.phoneUser2, "userClientPhone": chat.phoneUser1,
            "userLoggedImage": Decrypt(chat.imageUser2), "userClientImage": Decrypt(chat.imageUser1),
            "userLoggedEmail": Decrypt(chat.emailUser2), "userClientEmail": Decrypt(chat.emailUser1),
            "userLoggedBlockeds": chat.blockedsUser2, "userClientBlockeds": chat.blockedsUser1,
            "news": chat.news
        };
    }
}
