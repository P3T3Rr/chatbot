export const createUserAdapter = (data, id) => {
    const date = new Date(data.date.seconds * 1000);
    const months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]

    return {
        "id": id,
        "name": String(data.name), 
        "phone": String(data.phone), 
        "image": String(data.image),
        "email": String(data.email),
        "password": String(data.password),
        "blockeds": data.blockeds,
        "stadistics": data.stadistics,
        "date": String(date.getDate()+"/"+months[date.getMonth()]+"/"+date.getFullYear())
    };
} 