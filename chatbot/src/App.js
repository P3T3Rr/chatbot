import { collection, onSnapshot, query, where } from "firebase/firestore";
import { ColumnOne } from './components/column-one/column-one';
import { LoadingPage } from './components/loading-page/page';
import { createChatsAdapter } from "./adapters/chat-adapter";
import { createUserAdapter } from "./adapters/user-adapter";
import { Login } from "./components/login/login";
import { db } from './services/firebaseConfig';
import { useEffect, useState } from 'react';
import { Routing } from './routes/route';

export default function App() {

  const init = () => {
    const UserLogged = localStorage.getItem("user");
    return UserLogged ? JSON.parse(UserLogged) : [];
  }

  const UserLogged = init();
  const [logged, setLogged] = useState(false);
  const [spinner, setSpinner] = useState(true);
  const [user, setUser] = useState();
  const [chats, setChats] = useState();
  const [chats1, setChats1] = useState([]);
  const [chats2, setChats2] = useState([]);

  useEffect(() => {
    functionGetUser();
    functionGetChats1();
    functionGetChats2();
    if (UserLogged != '') {
      setLogged(true);
    }
  }, [UserLogged])

  useEffect(() => {
    if (chats !== null) {
      const list = [...chats1, ...chats2];
      list.sort(((a, b) => a.dateTimestamp - b.dateTimestamp))
      list.reverse()
      setChats(list);
    }
  }, [chats1, chats2])


  const functionGetUser = () => {
    const snap = query(collection(db, "users"), where("phone", "==", UserLogged));
    onSnapshot(snap, (querySnapshot) => {
      console.log("-----------------------------------------------\nLlamo a un user logueado: ");
      const users = [];
      querySnapshot.forEach((doc) => {
        if (doc.data() !== null) {
          users.push(createUserAdapter(doc.data(), doc.id));
          setUser(users[0]);
          setSpinner(false);
        }
        else {
          setUser(null);
        }
        console.log(users[0]);
      });
    });
  }

  const functionGetChats1 = () => {
    const snap = query(collection(db, "chats"), where("phoneUser1", "==", UserLogged));
    onSnapshot(snap, (querySnapshot) => {
      setChats1([]);
      console.log("-------------------------------\nLlamo a los chats 1: ");
      const newchats = [];
      querySnapshot.forEach((doc) => {
        if (doc.data() !== null) {
          newchats.push(createChatsAdapter(doc.id, doc.data()));
          setChats1(newchats);
        }
      });
      console.log(newchats);
    });
  }

  const functionGetChats2 = () => {
    const snap = query(collection(db, "chats"), where("phoneUser2", "==", UserLogged));
    onSnapshot(snap, (querySnapshot) => {
      setChats2([]);
      console.log("-------------------------------\nLlamo a los chats 2: ");
      const newchats = [];
      querySnapshot.forEach((doc) => {
        if (doc.data() !== null) {
          newchats.push(createChatsAdapter(doc.id, doc.data()));
          setChats2(newchats);
        }
      })
      console.log(newchats);
    });
  }

  return (
    <div className="App">
      {logged ?
        !spinner ?
          <>
            <ColumnOne user={user} chats={chats} setLogged={setLogged} />
            <Routing />
          </>
          :
          <LoadingPage />
        :
        <Login setLogged={setLogged} />
      }
    </div>
  );
}