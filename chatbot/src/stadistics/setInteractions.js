import { collection, getDocs, query, where } from "firebase/firestore";
import { Decrypt } from "../services/crypto-js";
import { db } from "../services/firebaseConfig";
import { getStadistics } from "../services/get";
import { updateStadistics } from "../services/update";

export const setinteractions = async ( logged , id) => {
    try {
        const jsonStruct = [];
        const data = await getDocs(query(collection(db, "chats"), where("phoneUser2", "==", logged)));
        data.forEach(doc => {
            jsonStruct.push(
                {
                    cantMessages: doc.data().cantMessages, 
                    userName: doc.data().nameUser1
                });
        });
        const data2 = await getDocs(query(collection(db, "chats"), where("phoneUser1", "==", logged)));
        data2.forEach(doc => {
            jsonStruct.push(
                {
                    cantMessages: doc.data().cantMessages, 
                    userName: doc.data().nameUser2
                });
        });
        const orderedStruct = jsonStruct.sort((a, b) => {
            return a["cantMessages"] > b["cantMessages"];
        })
        let stadistics = getStadistics(logged);
            stadistics.then(function (result) {
                result[5] = Decrypt(orderedStruct[0].userName);
                result[6] = Decrypt(orderedStruct[orderedStruct.length-1].userName);
                updateStadistics(id, result)
            }) 
    } catch (error) {
        console.log(error);
    }
}

