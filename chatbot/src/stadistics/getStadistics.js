export const getStadistics = async (stadistics) => {
    try {
        return {
            messages: stadistics[0],
            images: stadistics[1],
            videos: stadistics[2],
            audios: stadistics[3],
            words: stadistics[4],
            moreInteraction: stadistics[5],
            lessInteraction: stadistics[6]
        };
    }
    catch (error) {
        console.log(error);
    }
}