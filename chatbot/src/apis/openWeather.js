import { getDate } from '../validations/getDate'

var latitude = "";
var longitude = "";

async function showLocation(position) {
  latitude = position.coords.latitude;
  longitude = position.coords.longitude;
}

function errorHandler(err) {
  if (err.code === 1) {
    console.log("Error: Access is denied!");
  } else if (err.code === 2) {
    console.log("Error: Position is unavailable!");
  }
}

export const getWeather = async (i) => {
  if (i === 5) {
    return "En este momento no se \npuede consultar el clima.";
  }

  if (navigator.geolocation) {
    var options = { timeout: 60000 };
    navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);

    try {
      const api_call = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&lang=es&APPID=6938cff3ef87f376763afdd1e4e52447&units=metric`);
      const data = await api_call.json();
      let message = "Ciudad: " + data.name + ", " + data.sys.country + "\nClima: ";
      if (data.weather[0].description === 'nubes') {
        message += "nublado";
      }
      else if (data.weather[0].description === 'nubes dispersas') {
        message += "parcialmente nublado";
      }
      else if (data.weather[0].description === 'sol') {
        message += "soleado";
      }
      else {
        message += data.weather[0].description;
      }

      const date = new Date();;
      message += "\nTemperatura: " + data.main.temp + "°C."
        + "\nHumedad: " + data.main.humidity + "%"
        + "\nVel. viento: " + Math.round((data.wind.speed) * 10) + "km/h"
        + "\nFecha: " + date.toLocaleDateString('es-CR')
        + "\nHora: " + date.toLocaleTimeString('es-CR');

      console.log("Este es el intento: " + i);
      return message;

    } catch (error) {
      i += 1;
      return getWeather(i);
    }
  } else {
    return "¡Lo sentimos! \nEl navegador no es compatible\n con la geolocalización";
  }
}

