import { Route, Routes } from 'react-router-dom';
import { Chat } from '../components/column-two/chat/chat';
import { Principal } from '../components/column-two/home';

export const Routing = () => (
    <Routes>
        <Route path='/' element={<Principal />} />
        <Route path='/:id/:phone' element={<Chat />} />
    </Routes>
)